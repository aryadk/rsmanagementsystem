package com.arya.dokter;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface DokterRepository extends CrudRepository<Dokter, Long>{

}
