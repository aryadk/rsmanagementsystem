package com.arya.dokter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.arya.dokter.DokterService;
import com.arya.dokter.Dokter;

@Controller
public class DokterController {
	@Autowired
	DokterService dokterService;
	@RequestMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("index");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value="/getAll", method = RequestMethod.GET)
	public String getAll() {
		Gson gson = new Gson();
		List<Dokter> listOfDokter = dokterService.getAllDokter();
		return gson.toJson(listOfDokter);
	}
	
	@ResponseBody
	@RequestMapping(value="/detail", method = RequestMethod.GET)
	public String getById(@RequestParam(value = "id") long id) {
		Gson gson = new Gson();
		Dokter detdokter = dokterService.get(id);
		return gson.toJson(detdokter);
	}
}
