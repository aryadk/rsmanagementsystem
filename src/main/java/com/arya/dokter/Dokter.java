package com.arya.dokter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Dokter {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idDokter;
	private String namaDokter;
	private String spesialis;
	
	public Dokter() {
		super();
	}
	public Dokter(Long idDokter, String namaDokter, String spesialis) {
		super();
		this.idDokter = idDokter;
		this.namaDokter = namaDokter;
		this.spesialis = spesialis;
	}
	public Long getIdDokter() {
		return idDokter;
	}
	public void setIdDokter(Long idDokter) {
		this.idDokter = idDokter;
	}
	public String getNamaDokter() {
		return namaDokter;
	}
	public void setNamaDokter(String namaDokter) {
		this.namaDokter = namaDokter;
	}
	public String getSpesialis() {
		return spesialis;
	}
	public void setSpesialis(String spesialis) {
		this.spesialis = spesialis;
	}
	
	
}
