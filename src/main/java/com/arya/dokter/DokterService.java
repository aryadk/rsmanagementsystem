package com.arya.dokter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class DokterService {
	@Autowired
	DokterRepository dokterRepo;
	
	public List<Dokter> getAllDokter(){
		return (List<Dokter>) dokterRepo.findAll();
	}
	
	public Dokter get(Long id) {
		return dokterRepo.findById(id).get();
	}
}
