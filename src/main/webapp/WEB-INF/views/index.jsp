<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

</head>
<body>
	<div class="container">
		<table class="table">
			<thead>
				<tr>
					<th style="width: 10%">ID</th>
					<th style="width: 30%">Nama</th>
					<th style="width: 30%">Spesialis</th>
					<th class="text-center" style="width: 10%" colspan=3>Action</th>
				</tr>
			</thead>
			<tbody id="tb_dokter">
			</tbody>
		</table>
		${detdokter.namaDokter}
		<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
	</div>
	<div class="modal fade" id="modal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<table class="table" id="tb_detail">
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
s
		</div>
	</div>
</body>
<script>
$(document).ready(function(){
	getAll();
});
function getAll(){
	$.ajax({
		url : "${pageContext.request.contextPath}/getAll",
		type : "get",
		dataType : "json",
		success : function(data) {
			if(data.length > 0){
			for(var i = 0 ;i < data.length ; i++){
			$("#tb_dokter").append(
				"<tr>"
					+"<td>"+data[i].idDokter+"</td>"
					+"<td>"+data[i].namaDokter+"</td>"
					+"<td>"+data[i].spesialis+"</td>"
					+"<td><a href='#' onClick='showDetail("+data[i].idDokter+")'>Detail</a></td>"
					+"<td><a href='#' onClick='showEdit("+data[i].idDokter+")'>Edit</a></td>"
					+"<td><a href='#' onClick='delete()'>Delete</a></td>"
				+"</tr>"
			);}
			}
		},
		error : function(e) {
			alert('gagal');
		}
		
	});
}
function showModal() {
	$('#modal').modal({
		backdrop : 'static',
		keyboard : true,
		show : true
	});

	$('#modal').on('shown.bs.modal', function() {
		$('#name').focus();
	});
}
	function showDetail(id){
		$.ajax({
			url : "${pageContext.request.contextPath}/detail",
			type : "get",
			dataType : "json",
			data : {
				id : id
			},
			success : function(data) {
				$("#tb_detail").append(
					"<tr>"
					+	"<td>ID</td>"
					+	"<td >"
					+	data.idDokter
					+	"</td>"
					+	"</tr>"
					+	"<tr>"
					+	"<td>Nama</td>"
					+	"<td >"
					+	data.namaDokter
					+	"</td>"
					+	"</tr>"
					+	"<tr>"
					+	"<td>Spesialis</td>"
					+	"<td >"
					+	data.spesialis
					+	"</td>"
					+"</tr>"		
				);
				showModal(); 
			},
			error : function(e) {
				alert('gagal');
			}
			
		});
	}
</script>
</html>